<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'C:\wamp64\www\strictlydents\wp-content\plugins\wp-super-cache/' );
define( 'DB_NAME', 'strictlydents' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'SBUTy8bqSH;8+8nzjWth Ax]>[|yMw3.WIAkZ_L&k8u}Pi2vk*2~B@GZWEj`c`w;' );
define( 'SECURE_AUTH_KEY',  '_-|}hul<a)os,JAE:I2;GmQd*&|he63^Wpa72pI_t%P5p+df$o<vycvt5^0u!`~F' );
define( 'LOGGED_IN_KEY',    'yQ9 zb}4lm[+v?anzJm$KlI,TQk.>^<U? 2mn@t=oyCoxXjo&mK=Lh[![+to=F@1' );
define( 'NONCE_KEY',        'qlP_:imTV![]nB%YNmy%in-S9~<+#Ir>nF~RW|o!u!pZB985BML>hljSONq8NlqN' );
define( 'AUTH_SALT',        'pV5{b1q~Z CE*% G0gn?b}uc?eH74Dq,H9!nA|}LnLQ8!<KbC|-y#`>s1}i$PsaA' );
define( 'SECURE_AUTH_SALT', 'k%/M;m160b#`Ge_1P`|b|cJ]ib:!HM7w=jK{D3tqT^)Bmw%u5z}U^^}66i#rHE!}' );
define( 'LOGGED_IN_SALT',   'U,3+Qj#H1lfh>F6=VV>m-IPL=L@`<}q69h9lf$$|*c+!{i5X[8B~yK0NE&-W0C8v' );
define( 'NONCE_SALT',       '(SCW(3s-;0f.Bn8J4nULZ{<=RcR/g@(RR0/7-Kw[Q=TE8a2*2%Mx:L1H$hF1#7n ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
